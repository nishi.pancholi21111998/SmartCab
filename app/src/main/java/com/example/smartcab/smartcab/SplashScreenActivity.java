package com.example.smartcab.smartcab;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

public class SplashScreenActivity extends AppCompatActivity {

    // SPLASH_TIMEOUT defines the timeout to redirect to login/register activity.
    private final static int SPLASH_TIMEOUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        waitAndRedirect();
    }

    private void waitAndRedirect() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                Log.d("SMARTCAB", "Main Activity Started");
            }
        },SplashScreenActivity.SPLASH_TIMEOUT);
    }
}
